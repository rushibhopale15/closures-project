 function counterFunction() {
    let counter =0;
    
    return {
        increment: () => {
            return ++counter;
        },
        decrement : () =>{
            return --counter;
        }
    }
    
}

module.exports = counterFunction;