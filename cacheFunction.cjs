function cacheFunction(callBack){
    let cacheObj ={};
    if(callBack === undefined)
    {
        throw new Error("Invalid Parameters");
    }
    return function(...args){

        if(cacheObj.hasOwnProperty(args)){
            console.log("Already there");
        return cacheObj[args];
        }
        else
        {
            cacheObj[args] = callBack(...args);
            console.log("new function");
            return cacheObj[args];
        }
    }
}

module.exports = cacheFunction;

