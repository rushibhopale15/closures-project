let limitFunctionCallCount = require('./../limitFunctionCallCount.cjs');

let callBack = () => {
    return `hello function called`;
}

try {
let functionCall = limitFunctionCallCount(callBack,3)
    console.log(functionCall());
    console.log(functionCall());
    console.log(functionCall());
    console.log(functionCall());
    console.log(functionCall());
}
catch (error) {
    console.log(`error is ${error}`);
}