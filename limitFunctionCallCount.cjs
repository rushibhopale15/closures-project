
function limitFunctionCallCount(callBack, limit)
{
    let count = 0;
    if(callBack === undefined || limit === undefined || limit === null)
    {
        throw console.error(" invalid parameters ");
    }
    return function (...args){
        if(count < limit)
        {
            count++;
            return callBack(...args);
        }
        else
        {
            return null;
        }
    };
}

module.exports = limitFunctionCallCount;